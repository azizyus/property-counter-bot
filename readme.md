# DOC


## Ne işe yarar?

bots_config içerisinde belirtilmiş web sitelerinde, belirli bir keyword sayesinde
ulaştığınız sayfalardaki belirli bir firmaya ait ilanları sayar.

## Nası çalışır?

Stroage/App/Bots klasörü içerisindeki json dosyalarının içeriğine göre hangi bot
kaç sayfa çalışacak ve neyi baz alarak arama yapacağını belirler ve buna göre çalışır.

## Bahsi geçen json dosyası
Belirtilen örnek x.json olarak kaydedildiği vakit aşağıdaki bilgilerce işlevini 
yerine getirecektir ve çalışması için
<code>php artisan bots:unleash --bot-file-name=x.json</code> kullanılabilir, bu komut tek bir dosyayı 
işleyecek ve  içerisindeki tüm görevleri yerine getirecektir. Console/Kernel içersinde tüm dosyaları yakalayıp
tek tek işleyecek örnek zaten var olduğundan sadece cronjob eklemek kafi gelecektir.
<pre>
{
         "schedule" : {
      
            "time" :  "12:00",
            "frequency" : "daily"
      
          },
      
          "recipients": [
           "test@mail.com"
          ],
          "keywords": [
            "firma_adi",
            "firmaadi"
          ],
          "searchString" : "x+satılık",
          "bots": 
          {
                "sahibinden": {
                  "start": 1,
                  "end": 50,
                  "interval" : 1
                },
                "milliyet":
                {
                  "start": 1,
                  "end" : 50
                },
                "hurriyet":
                {
                    "start":1,
                    "end":50,
                    "interval" : 3
                },
                "zingat":
                {
                    "start":1,
                    "end":50
                }
         }  
}
</pre>

### bahsi geçen json dosyasında bilinmesi gerekenler
schedule çalışma aralığı belirler örnek içerisinde belirtiği gibi kullanabilirsiniz lakin halen dökümana eklenmemiş 
bir tanıma daha sahiptir.

<br>
recipients, raporun sonucunu alacak kişilerin listesini tanımlar.

<br>
keywords, aranacak firmanın adını veya adına benzeyen kelimeleri içerir, ilan içerisinde her hangi biri bulunursa sayaç artar.

<br>
searchString, bahsi geçen sitede hangi kelimeye ait sonuçların aranmasını istiyorsanız o kelime belirtilir.
<br>
bots, bots_config içerisinde tanımlanmış botlara ait keyler ile bu json dosyasındakiler eşleşerek hangi botun ne kadar çalışacağı belirtilir.
<br>
start: başlangıç sayfası <br>
end: bitiş sayfası <br>
interval: iki request arası boşluk, http 429dan kurtulmaya yarar.
<br>

