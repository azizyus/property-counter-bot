<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 9:59 PM
 */

namespace App\Bots\Mail;


use App\Bots\Counter\PageComposer;
use App\Bots\Notifications\ReportNotification;
use App\Logger\Logger;
use Illuminate\Support\Facades\Notification;

class MailHelper
{

    public function sendMail(PageComposer $pageComposer, String $mailTitle,array $recipients)
    {
        $pageCollection = $pageComposer->getPageCollection();
        Logger::log("a mail sent via this title: $mailTitle");

        try
        {
            Notification::route("mail",$recipients)->notify(new ReportNotification($pageCollection,$mailTitle));
        }
        catch (\Exception $exception)
        {
            Logger::log("couldnt send '$mailTitle' titled email");
            Logger::log($exception->getMessage());
        }
    }

}