<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:32 PM
 */

namespace App\Bots;


use App\Bots\StringHelper\KeywordCompareHelper;
use App\Bots\Traits\SimpleBotRunLoop;
use PHPHtmlParser\Dom;

abstract class DetailFinderBot extends AbstractBot
{


    public $detailPageOfficeSelector;
    use SimpleBotRunLoop;


    public function processListingPageBoxElement($listingBoxElement,$listingPageLink)
    {

        $detailPageLinks = $listingBoxElement->find($this->detailLinkSelector);

        foreach ($detailPageLinks as $link)
        {

            $this->hasDetailHasAnySpecifiedKeyword($link->getAttribute("href"),$listingPageLink);
        }


    }


    public function hasDetailHasAnySpecifiedKeyword($detailPageUrlOrSlug,$listingPageLink)
    {


        $detailPageHtml = $this->getRequestListingPageHtml($this->client->completeLink($this->baseUrl(), $detailPageUrlOrSlug));
        if($this->hasOfficeName($detailPageHtml))
        {

            $this->pushPageItem($detailPageUrlOrSlug, $listingPageLink);

        }


    }

    public function hasOfficeName(String $detailPageHtml) : Bool
    {

        $foundOfficeNameElement = $this->htmlParser->getFirst($this->detailPageOfficeSelector,$detailPageHtml);
        if($foundOfficeNameElement!=null && KeywordCompareHelper::has($foundOfficeNameElement->text,$this->keywords)) return true;
        else return false;

    }



}