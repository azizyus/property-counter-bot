<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:00 PM
 */

namespace App\Bots;


use App\Bots\Counter\PageItem;
use App\Bots\Counter\PageComposer;
use App\Dom\PhpHtmlParser;
use App\Guzzle\Factory;
use App\Guzzle\GuzzleWrapper;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;

abstract class AbstractBot
{

    // page => count
    public $pageComposer;

    protected $client = null;

    protected $startPage = 1;
    protected $endPage = 0;
    public $searchString="";
    public $keywords=[];

    protected $currentPage;

    public $listingPageDetailBoxSelector=null;
    public $detailLinkSelector="";
    protected $htmlParser;
    public function __construct()
    {
        $this->pageComposer = new PageComposer();
        $this->htmlParser = new PhpHtmlParser();
        $this->client = Factory::make();
    }

    public function setStartPage(Int $page)
    {
        $this->startPage = $page;
        $this->currentPage = $page;
    }

    public function setEndPage($page)
    {
        $this->endPage = $page;
    }

    public function setHtmlParser(PhpHtmlParser $phpHtmlParser)
    {
        $this->htmlParser = $phpHtmlParser;
    }

    public function setClient(GuzzleWrapper $client)
    {
        $this->client = $client;
    }

    public function setSearchString(String $searchString)
    {
        $this->searchString = $searchString;
    }


    public function setKeywords(array $keywords)
    {
        $this->keywords = $keywords;
    }
    /*
     *
     * main loop of bot
     */
    abstract public function run();

    abstract public function getListingPageLink(Int $page, String $searchString);

    protected function pushPageItem($detailUrl = null, $listingUrl = null)
    {
        $pageItem = new PageItem($this->currentPage);

        $pageItem->setDetailUrl($detailUrl);
        $pageItem->setListingPageUrl($listingUrl);

        $this->pageComposer->addPageItem($pageItem);
    }


    public function getPageComposer() : PageComposer
    {
        return $this->pageComposer;
    }

    /**
     * @return String
     *
     * lets assume its www.google.com/test-me-slug
     * the return should be www.google.com
     *
     */
    abstract public function baseUrl() : String;


    public function getRequestListingPageHtml($link)
    {
        return $this->client->sendGetRequest($link);
    }


    public function getResponseBody($response) : String
    {
        return $this->client->getResponseBody($response);
    }

    public function getResponseHttpCode($response) : Int
    {
        return $this->client->getResponseHttpCode($response);
    }

    abstract public function processListingPageBoxElement($listingBoxElement,$listingPageLink);

    public function getListingPageDetailBoxElements($listingPageHtml)
    {
        return $this->htmlParser->getAll($this->listingPageDetailBoxSelector,$listingPageHtml);
    }

}