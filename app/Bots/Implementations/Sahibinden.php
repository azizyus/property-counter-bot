<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/30/18
 * Time: 7:09 PM
 */

namespace App\Bots\Implementations;


use App\Bots\DetailFinderBot;

class Sahibinden extends DetailFinderBot
{


    public $listingPageDetailBoxSelector = ".searchResultsItem"; //for sahibinden
    public $detailLinkSelector = ".classifiedTitle";
    public $detailPageOfficeSelector = ".storeInfo"; //detailFinder specific prop


    //https://www.sahibinden.com/emlak/aydin-kusadasi?pagingOffset=20&query_text_mf=ku%C5%9Fadas%C4%B1+sat%C4%B1l%C4%B1k&query_text=sat%C4%B1l%C4%B1k
    public function getListingPageLink(Int $page, String $searchString)
    {
        $pageOffset = $page*20;
        return "https://www.sahibinden.com/emlak?pagingOffset=$pageOffset&query_text=$searchString";
    }

    public function baseUrl(): String
    {
        return "https://www.sahibinden.com";
    }

}
