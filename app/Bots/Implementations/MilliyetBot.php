<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 2:20 PM
 */

namespace App\Bots\Implementations;


use App\Bots\ListingFinderBot;

class MilliyetBot extends ListingFinderBot
{

    public $listingPageDetailBoxSelector = ".propertyContainer"; //for milliyet
    public $listingPageOfficeSelector = ".owner"; //listingFinder specific prop
    public $detailLinkSelector = ".property";

    public function getListingPageLink(Int $page, String $searchString)
    {
        return "https://www.milliyetemlak.com/arama?arama=$searchString&sayfa=$page";
    }

    public function baseUrl(): String
    {
        return "https://www.milliyetemlak.com";
    }


}
