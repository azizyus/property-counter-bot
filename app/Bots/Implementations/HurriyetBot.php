<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:10 PM
 */

namespace App\Bots\Implementations;


use App\Bots\DetailFinderBot;

class HurriyetBot extends DetailFinderBot
{

    public $detailLinkSelector = ".overlay-link";
    public $listingPageDetailBoxSelector = ".list-container"; //for hürriyet
    public $detailPageOfficeSelector = ".ownerOffice-name a"; //detailFinder specific prop

    public function baseUrl(): String
    {
        return "https://www.hurriyetemlak.com";
    }

    public function getListingPageLink(Int $page, String $searchString)
    {
        return "https://www.hurriyetemlak.com/emlak?q=$searchString&page=$page";
    }



}