<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/3/19
 * Time: 8:51 PM
 */

namespace App\Bots\Implementations;


use App\Bots\DetailFinderBot;

class ZingatBot extends DetailFinderBot
{

    public $listingPageDetailBoxSelector = ".project-list div"; //for zingat
    public $detailLinkSelector = ".project-title a";
    public $detailPageOfficeSelector = ".agent-company-link"; //detailFinder specific prop

    public function baseUrl(): String
    {
        return "https://www.zingat.com";
    }

    public function getListingPageLink(Int $page, String $searchString)
    {
        return "https://www.zingat.com/satilik?page={$page}&page_size=20&keyword={$searchString}&listingTypeId=1&fromPage=true";
    }


}