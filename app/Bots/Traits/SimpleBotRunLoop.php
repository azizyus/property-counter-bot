<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 6:37 PM
 */

namespace App\Bots\Traits;


trait SimpleBotRunLoop
{
    
    public function run()
    {
        for ($this->currentPage=$this->startPage;$this->currentPage<=$this->endPage;$this->currentPage++)
        {

            $this->pageComposer->initNewPage($this->currentPage);
            $listingPageLink = $this->getListingPageLink($this->currentPage,$this->searchString);
            $listingPageHtml = $this->getRequestListingPageHtml($listingPageLink);


            $listingPageDetailBoxElements = $this->getListingPageDetailBoxElements($listingPageHtml);

            foreach ($listingPageDetailBoxElements as $element)
            {
                $this->processListingPageBoxElement($element,$listingPageLink);
            }

        }
    }

}