<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 7:21 PM
 */

namespace App\Bots\StringHelper;


class KeywordCompareHelper
{

    public static function has($text,array $possibilities)
    {
                            //tr characters like capital İ will be bad with normal strtolower
        return str_contains(mb_strtolower($text),array_map('mb_strtolower', $possibilities));
    }

}