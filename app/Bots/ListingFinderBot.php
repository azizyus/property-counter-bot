<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:17 PM
 */

namespace App\Bots;


use App\Bots\Counter\PageItem;
use App\Bots\StringHelper\KeywordCompareHelper;
use App\Bots\Traits\SimpleBotRunLoop;
use PHPHtmlParser\Dom;

abstract class ListingFinderBot extends AbstractBot
{

    use SimpleBotRunLoop;

    public $listingPageOfficeSelector="";


    public function processListingPageBoxElement($listingBoxElement,$listingPageLink)
    {

        $element = $listingBoxElement->find($this->listingPageOfficeSelector)[0];
        $detailLinkElement = $listingBoxElement->find($this->detailLinkSelector)[0];

        if($element!=null && KeywordCompareHelper::has($element->text,$this->keywords))
        {
            $detailLink = "cant_find_element";
            if($detailLinkElement) $detailLink = $detailLinkElement->getAttribute("href");

            $this->pushPageItem($detailLink, $listingPageLink);

        }

    }






}