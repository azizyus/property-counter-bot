<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 9:06 PM
 */

namespace App\Bots\ConfigHelper;


use App\Bots\AbstractBot;

class ConfigHelper
{

    public $config;
    public function __construct()
    {
        $this->config = config("bots_config.bots");
    }

    public  function getBotFromConfig($name)
    {

        $bots = $this->config;
        foreach ($bots as $key => $bot)
        {

            if($key == $name)
            {
                return $bot;

            }
        }

        throw new \Exception("I CANT FIND YOUR BOT FROM BOT.PHP CONFIG FILE");
    }

    public function getBotMailTitleFromConfig($name)
    {
        $botParams = $this->getBotFromConfig($name);
        $mailTitle = $botParams["mailTitle"];

        return $mailTitle;
    }

    public function getBotInstanceFromConfig($name) : AbstractBot
    {
        $botParams = $this->getBotFromConfig($name);
        $namespace = $botParams["namespace"];
        $instance = new $namespace();
        return $instance;
    }

}