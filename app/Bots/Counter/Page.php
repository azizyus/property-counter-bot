<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/30/18
 * Time: 5:22 PM
 */

namespace App\Bots\Counter;


class Page
{


    public $pageItems;
    public $pageNumber;

    public function __construct(Int $pageNumber)
    {
        $this->pageItems = collect();
        $this->pageNumber = $pageNumber;
    }

    public function getPageItemCount()
    {
        return $this->pageItems->count();
    }
    public function add(PageItem $counter)
    {
        $this->pageItems->push($counter);
    }

}