<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 9:28 PM
 */

namespace App\Bots\Counter;


class PageComposer
{


    protected $pageCollection;

    public function __construct()
    {
        $this->pageCollection = collect();
    }
    public function initNewPage(Int $pageNumber)
    {

        $this->pageCollection->push(new Page($pageNumber));

    }

    public function addPageItem(PageItem $pageItem)
    {

        $page = $this->pageCollection->where("pageNumber",$pageItem->pageNumber)->first();

        if($page)
        {

           $this->pageCollection->map(function ($page) use($pageItem){

                if($page->pageNumber == $pageItem->pageNumber)
                {
                    $page->add($pageItem);
                }

            });

        }
        else
        {
            $page = new Page($pageItem->pageNumber);
            $page->add($pageItem);
            $this->pageCollection->push($page);
        }




    }

    public function getPageCollection()
    {
        return $this->pageCollection;
    }

}