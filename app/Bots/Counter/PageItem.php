<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:20 PM
 */

namespace App\Bots\Counter;


class PageItem
{

    public $pageNumber;

    public $isSuccess=false;
    public $detailUrl;
    public $listingPageUrl;


    /**
     * PageItem constructor.
     *
     * @param Int $pageNumber
     */
    public function __construct(Int $pageNumber)
    {
        $this->pageNumber = $pageNumber;

    }

    /**
     * @return mixed
     */
    public function getDetailUrl()
    {
        return $this->detailUrl;
    }

    /**
     * @param mixed $detailUrl
     */
    public function setDetailUrl($detailUrl): void
    {
        $this->detailUrl = $detailUrl;
    }

    /**
     * @return mixed
     */
    public function getListingPageUrl()
    {
        return $this->listingPageUrl;
    }

    /**
     * @param mixed $listingPageUrl
     */
    public function setListingPageUrl($listingPageUrl): void
    {
        $this->listingPageUrl = $listingPageUrl;
    }


}