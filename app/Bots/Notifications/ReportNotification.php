<?php

namespace App\Bots\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReportNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $pages;
    public $mailTitle;
    public function __construct($pages,$mailTitle)
    {
        $this->pages = $pages;
        $this->mailTitle = $mailTitle;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $mailMessage = new MailMessage();
        $mailMessage->subject($this->mailTitle);
        $mailMessage->from(env("REPORT_NOTIFICATION_MAIL"),env("REPORT_NOTIFICATION_MAIL_NAME"));
        $mailMessage->greeting("Merhaba, hazırlanan rapor aşağıdaki gibidir;");

        $pages = $this->pages;



        foreach ($pages as $page)
        {
            $mailMessage->line("{$page->pageNumber}. sayfa içerisinde {$page->getPageItemCount()} adet ilan bulundu");
        }


        return $mailMessage;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
