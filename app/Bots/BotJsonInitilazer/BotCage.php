<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 9:34 PM
 */

namespace App\Bots\BotJsonInitilazer;


use App\Bots\ConfigHelper\ConfigHelper;
use App\Bots\Mail\MailHelper;
use App\Guzzle\Factory;

class BotCage
{

    public function unleash($json)
    {

        $configHelper = new ConfigHelper();
        $mailHelper = new MailHelper();
        foreach ($json["bots"] as $botName => $params)
        {

            //read and take instance of bots from json file

            $bot = $configHelper->getBotInstanceFromConfig($botName);


            $bot->setStartPage($params["start"]);
            $bot->setEndPage($params["end"]);
            $bot->setKeywords($json["keywords"]);
            $bot->setSearchString($json["searchString"]);


            if(isset($params["interval"]))
            {
                $bot->setClient(Factory::makeSpecificIntervalBot((Int)$params["interval"]));
            }
            $bot->run();
            $pageComposer = $bot->getPageComposer();
            $mailTitle = $configHelper->getBotMailTitleFromConfig($botName);
            $recipients = $json["recipients"];
            $mailHelper->sendMail($pageComposer,$mailTitle,$recipients);


        }
    }

}