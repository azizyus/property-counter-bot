<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/29/18
 * Time: 7:38 PM
 */

namespace App\Bots\BotJsonInitilazer;


use Illuminate\Support\Facades\Storage;

class JsonReader
{

    public $fileName;
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function readFile()
    {
        return BotFileHelper::readBotConfigFileByName($this->fileName);
    }

    public function parseContent(String $fileContent)
    {
        return json_decode($fileContent,true);
    }

    public function parseFile()
    {
        $fileContent = $this->readFile();
        return $this->parseContent($fileContent);
    }

}