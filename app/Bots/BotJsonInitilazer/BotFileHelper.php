<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/31/18
 * Time: 4:40 PM
 */

namespace App\Bots\BotJsonInitilazer;


use Illuminate\Support\Facades\Storage;

class BotFileHelper
{

    public static function allBotConfigFiles()
    {

        $files = Storage::files("bots");

        return $files;

    }

    public static function readBotConfigFileByName($fileName)
    {
        return Storage::get("bots/{$fileName}");
    }

}