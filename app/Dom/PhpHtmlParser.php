<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/30/18
 * Time: 7:52 PM
 */

namespace App\Dom;


use App\Logger\Logger;
use PHPHtmlParser\Dom;

class PhpHtmlParser
{



    public function getAll($selector,$str)
    {

        $dom = new Dom();
        $dom->load($str);
        $multipleThings = $dom->find($selector);
        if($multipleThings==null) Logger::log("i cant find this selector $selector");
        unset($dom);
        return $multipleThings;

    }

    public function getFirst($selector,$str)
    {
        $dom = new Dom();
        $dom->load($str);
        $singleThing = $dom->find($selector)[0]; //it should be user post, it doesnt have to be company post so its fine
        unset($dom);
        return $singleThing;
    }

}