<?php

namespace App\Console\Commands;

use App\Bots\BotJsonInitilazer\BotCage;
use App\Bots\BotJsonInitilazer\JsonReader;
use Illuminate\Console\Command;

class UnleashBots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bots:unleash {--bot-file-name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $fileName = $this->option("bot-file-name");

        $jsonReader = new JsonReader($fileName);

        $json = $jsonReader->parseFile();
        $botCage = new BotCage();
        $botCage->unleash($json);
    }
}
