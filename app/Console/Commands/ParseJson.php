<?php

namespace App\Console\Commands;

use App\Bots\BotJsonInitilazer\JsonReader;
use App\Bots\ConfigHelper\ConfigHelper;
use Illuminate\Console\Command;

class ParseJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jsonReader = new JsonReader("bot.json");

        $file = $jsonReader->readFile();
        $json = $jsonReader->parseContent($file);
        $configHelper = new ConfigHelper();
        foreach ($json["bots"] as $botName => $params)
        {

            //read and take instance of bots from json file

            $bot = $configHelper->getBotInstanceFromConfig($botName);


            $bot->setStartPage($params["start"]);
            $bot->setEndPage($params["end"]);
            $bot->setKeywords($json["keywords"]);
            $bot->setSearchString($json["searchString"]);

            $bot->run();
            dd($bot->getPageComposer());

        }

    }
}
