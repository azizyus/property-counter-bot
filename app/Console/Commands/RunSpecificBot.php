<?php

namespace App\Console\Commands;

use App\Bots\Implementations\HurriyetBot;
use App\Bots\Implementations\MilliyetBot;
use Illuminate\Console\Command;

class RunSpecificBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:specific:bot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        $bot = new MilliyetBot();
        $bot = new HurriyetBot();

        $bot->setStartPage(1);
        $bot->setEndPage(1);

        $bot->searchString = "kuşadası+satılık";

//        $bot->keywords = ["manor","manor gayrimenkul"]; //milliyet
        $bot->keywords = ["REMAX","REMAX PASHA"]; //hürriyet


//        $bot->listingPageDetailBoxSelector = ".propertyContainer"; //for milliyet
//        $bot->listingPageOfficeSelector = ".owner"; //listingFinder specific prop
//        $bot->detailLinkSelector = ".property";
//
        $bot->detailLinkSelector = ".overlay-link";
        $bot->listingPageDetailBoxSelector = ".list-container"; //for hürriyet
        $bot->detailPageOfficeSelector = ".ownerOffice-name a"; //detailFinder specific prop


        $bot->run();

            dd($bot->getPageComposer());

    }
}
