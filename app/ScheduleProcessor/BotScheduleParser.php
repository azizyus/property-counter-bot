<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/31/18
 * Time: 4:19 PM
 */

namespace App\ScheduleProcessor;


use App\Bots\BotJsonInitilazer\BotFileHelper;
use App\Bots\BotJsonInitilazer\JsonReader;
use App\Logger\Logger;
use App\ScheduleProcessor\Enums\ScheduleEnums;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Storage;

class BotScheduleParser
{


    public static function allFiles(Schedule $cronSchedule)
    {


        $files = BotFileHelper::allBotConfigFiles();

        foreach ($files as $file)
        {

            $fileName = basename($file);
            $jsonReader = new JsonReader($fileName);
            $jsonDecoded = $jsonReader->parseFile();

            $jsonSchedule = $jsonDecoded["schedule"];

            $frequency = $jsonSchedule["frequency"];


            if($frequency == ScheduleEnums::_DAILY)
            {
                $cronSchedule->command("bots:unleash --bot-file-name={$fileName}")->dailyAt($jsonSchedule["time"]);
                Logger::log("$fileName touched at {$jsonSchedule["time"]} as daily job");
            }
            elseif($frequency == ScheduleEnums::_WEEKLY)
            {
                $cronSchedule->command("bots:unleash --bot-file-name={$fileName}")->weeklyOn((int)$jsonSchedule["day"]);
                Logger::log("$fileName touched at {$jsonSchedule["day"]} as weekly job");
            }
            elseif($frequency == ScheduleEnums::_HOURLY)
            {
                $cronSchedule->command("bots:unleash --bot-file-name={$fileName}")->hourlyAt((int)$jsonSchedule["time"]);
                Logger::log("$fileName touched at {$jsonSchedule["time"]} as hourly job");
            }



        }



    }


}