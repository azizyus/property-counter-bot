<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/31/18
 * Time: 4:51 PM
 */

namespace App\ScheduleProcessor\Enums;


class ScheduleEnums
{


    const _DAILY = "daily";
    const _WEEKLY = "weekly";
    const _HOURLY = "hourly";

}