<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:26 PM
 */

namespace App\Guzzle;


use GuzzleHttp\Client;

class Factory
{

    public static function make() : GuzzleWrapper
    {
        return new GuzzleWrapper();
    }

    public static function makeSpecificIntervalBot(Int $interval) : GuzzleWrapper
    {
        $guzzleWrapper = self::make();
        $guzzleWrapper->setInterval($interval);

        return $guzzleWrapper;

    }

    public static function makeOneSecondIntervalBot() : GuzzleWrapper
    {

        $guzzleWrapper = self::make();
        $guzzleWrapper->requestInterval = 1;

        return $guzzleWrapper;

    }

    public static function makeFiveSecondIntervalBol() : GuzzleWrapper
    {
        $guzzleWrapper = self::make();
        $guzzleWrapper->requestInterval = 5;
        return $guzzleWrapper;
    }

    public static function makeTenSecondIntervalBol() : GuzzleWrapper
    {
        $guzzleWrapper = self::make();
        $guzzleWrapper->requestInterval = 10;
        return $guzzleWrapper;
    }

}