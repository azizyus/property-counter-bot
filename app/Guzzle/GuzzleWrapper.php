<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/28/18
 * Time: 9:27 PM
 */

namespace App\Guzzle;


use App\Logger\Logger;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class GuzzleWrapper
{


    public $client;
    public $requestInterval=0;

    public function __construct()
    {
        $this->client = new Client([
            "headers"=>[

                'User-Agent' => "Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0"

            ]
        ]);

    }

    public function setInterval(Int $interval)
    {
        $this->requestInterval = $interval;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function sendGetRequest(String $url,$parameters=[])
    {

            if($this->requestInterval>0)
            {
                sleep($this->requestInterval);
                Logger::log("i slept $this->requestInterval sec");
            }
            $resultHtml = "";
            try
            {
                $res = $this->client->get($url,$parameters);
                $resHttpCode = $this->getResponseHttpCode($res);
                Logger::log("[HTTP:$resHttpCode]requested url => $url");
                if($resHttpCode == HttpEnums::_HTTP_OK) $resultHtml = $this->getResponseBody($res);;
                return $resultHtml;
            }
            catch (RequestException $exception)
            {
                $resHttpCode = $exception->getResponse()->getStatusCode();
                Logger::log("[HTTP:$resHttpCode]requested url => $url");
            }


            return $resultHtml;



    }
    
    public function completeLink($baseUrl, $url)
    {
        $hasHttp = str_contains($url,"http://");

        if($hasHttp)
        {
            return $url;
        }
        else
        {
            return $baseUrl.$url;
        }

    }

    public function getResponseBody($response) : String
    {
        return $response->getBody()->getContents();
    }

    public function getResponseHttpCode($response) : Int
    {
        return $response->getStatusCode();
    }





}