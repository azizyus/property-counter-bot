<?php


return [


    "bots" => [

        "hurriyet" => [

            "enum" => \App\Bots\Enums\BotEnums::_HURRIYET,
            "namespace" => \App\Bots\Implementations\HurriyetBot::class,
            "mailTitle" => "Hürriyet Bot Günlük İstatistik",

        ],
        "sahibinden" => [
            "enum" => \App\Bots\Enums\BotEnums::_SAHIBINDEN,
            "namespace" => \App\Bots\Implementations\Sahibinden::class,
            "mailTitle" => "Sahibinden Bot Günlük İstatistik",
        ],
        "milliyet" => [

            "enum" => \App\Bots\Enums\BotEnums::_MILLIYET,
            "namespace" => \App\Bots\Implementations\MilliyetBot::class,
            "mailTitle" => "Milliyet Bot Günlük İstatisik",

        ],
        "zingat" => [

            "enum" => \App\Bots\Enums\BotEnums::_ZINGAT,
            "namespace" => \App\Bots\Implementations\ZingatBot::class,
            "mailTitle" => "Zingat Bot Günlük İstatisik",

        ],
    ],



];